"""
	STRipy
	Copyright (C) 2021  Andreas Halman

	STRipy is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	STRipy is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with STRipy.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import csv
import sys
import json
import regex
import pysam
import base64
import hashlib
import argparse
import tempfile
import subprocess
import numpy as np
import multiprocessing
from subprocess import call
from functools import partial
from datetime import datetime
from collections import defaultdict

__author__ = "Andreas Halman"
__contact__ = "dev@stripy.org"
__url__ = "https://stripy.org"
__publication__ = "https://doi.org/10.1002/humu.24382"
__copyright__ = "Copyright (C) 2021, Andreas Halman"
__license__ = "GPLv3"
__version__ = "2.5.3"

scriptpath = os.path.dirname(os.path.abspath(__file__))

def executableFileExists(exfile):
	""" Checks if executable file exists
	Args:
		exfile (file): Input file

	Yields:
		bool: True if exists and False if not
	"""

	return subprocess.call("type " + exfile, shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE) == 0

def read_pair_generator(bam, read_dict):
	"""Generate read pairs

	Args:
		bam (file): Input BAM file
		read_dict (dict): Directory which contains names of reads

	Yields:
		str: Yields read1 and read2
	"""
	for read in bam.fetch():
		if not read.is_paired or read.is_secondary or read.is_supplementary:
			continue

		qname = read.query_name
		if qname not in read_dict:
			if read.is_read1:
				read_dict[qname][0] = read
			elif read.is_read2:
				read_dict[qname][1] = read
		else:
			if read.is_read1:
				yield read, read_dict[qname][1]
			elif read.is_read2:
				yield read_dict[qname][0], read
			del read_dict[qname]

def bam2fast(inputfile, outputfolder):
	"""Converts BAM file to FASTQ files

	Args:
		inputfile (file): BAM file
		outputfolder (str): Output folder where FASTQ files will be saved
	"""
	bamfile = pysam.AlignmentFile(inputfile, "rb", ignore_truncation = True)

	# Extract out reads and save as FASTQ
	fastq1_reads = []
	fastq2_reads = []

	read_dict = defaultdict(lambda: [None, None])
	for read1, read2 in read_pair_generator(bamfile, read_dict):
		if read1 and read2:
			fastq1_reads.append(">%s\n%s\n+\n%s\n" % (read1.query_name, read1.query_sequence, "".join(map(lambda x: chr( x+33 ), read1.query_qualities))))
			fastq2_reads.append(">%s\n%s\n+\n%s\n" % (read2.query_name, read2.query_sequence, "".join(map(lambda x: chr( x+33 ), read2.query_qualities))))

	# Write FASTQ file
	inputfilename = os.path.splitext(os.path.basename(inputfile))[0]

	with open(outputfolder + inputfilename + ".regions_R1.fq", "w") as fastq_out:
		for r in fastq1_reads:
			fastq_out.write(r)

	with open(outputfolder + inputfilename + ".regions_R2.fq", "w") as fastq_out:
		for r in fastq2_reads:
			fastq_out.write(r)

	return True


def loadCatalog():
	with open(os.path.join(scriptpath, 'catalog.json'), mode='r') as cat_file:
		catalog = json.load(cat_file)

	return catalog

def getDiseaseInfo(req_locus):
	"""Find the matching disease or locus/gene in the catalogue and return all data for the matching locus

	Args:
		req_locus (str): Name of the locus

	Returns:
		dict: Variant information, or return False if nothing was found
	"""

	catalog = loadCatalog()

	for entry in catalog:
		entry['Locus'] = entry['Locus'].replace(":", "-") # Need to replace, otherwise HTML does not work
		matching_locus = True if entry['Locus'] == req_locus else False

		if matching_locus:
			entry.update({
				'MotifLength': len(entry['MotifPlusStrand']),
			})
			return entry

	else:
		return False


def readCatalogue(locus_name):
	"""Load the catalogue on disk and return the data about the requested disease and locus (gene)

	Args:
		locus_name (str): Locus name (in majority of cases gene's abbreviation)

	Returns:
		dict: Data from the catalogue about the locus matching with requested disease
	"""

	catalog = loadCatalog()

	for entry in catalog:
		entry['Locus'] = entry['Locus'].replace(":", "-") # Need to replace, otherwise HTML does not work
		if entry['Locus'] == locus_name:
			return entry
	
	return False # Could not find the specified gene/locus


def validateInput(input_text):
	"""Check whether an input text is alphanumeric (and can also include dash)

	Args:
		input_text (str): Input text

	Returns:
		str: Return the input text or empty string if it is not alphanumeric
	"""

	return input_text if regex.match(r'^\w+[-]?\w+$', input_text) else ''


def alleleSplitToNumArray(string, split_char):
	"""Split a string of genotypes to integer list

	Args:
		string (str): Alleles' genotype as a string (e.g. 10/20)
		split_char (str): Character separating genotypes (e.g. /)

	Returns:
		list: Genotypes as a list
	"""

	return list(map(int, string.split(split_char)))


def determineDiagnose(repeat_length, normal_range, intermediate_range, path_range_min):
	"""Based on the range of repeats specified in the catalogue, decide whether the repeat is normal, pathogenic, intermediate or unknown

	Args:
		repeat_length (int): Number of repeats in the allele
		normal_range (dict): Maximum value of the normal range
		intermediate_range (dict): Minimum and maximum value of the intermediate range
		path_range_min (int): Pathogenic cut-off value

	Returns:
		str: Returns whether the repeat length belongs into normal, intermediate, pathogenic or unknown range
	"""

	if repeat_length >= normal_range['Min'] and repeat_length <= normal_range['Max']:
		diagnose = "normal"
	elif repeat_length >= path_range_min:
		diagnose = "pathogenic"
	else:
		if intermediate_range != "NA":
			if repeat_length >= intermediate_range['Min'] and repeat_length <= intermediate_range['Max']:
				diagnose = "intermediate"
			else:
				diagnose = "unknown"
		else:
			diagnose = "unknown"

	return diagnose


def replaceSVGColours(content, normal_range, intermediate_range, path_range_min):
	"""Replace colours in the REViewer's output SVG file based on the "diagnose"

	Args:
		content (str): SVG file content
		normal_range (dict): Maximum value of the normal range
		intermediate_range (dict): Minimum and maximum value of the intermediate range
		path_range_min (int): Pathogenic cut-off value

	Returns:
		str: SVG file content after colourisation
	"""

	repeat_units = int(regex.search(r'(\d*) units', content).group(1))

	if determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "normal":
		section_new = regex.sub('#fc8d62', '#71bf2c', content)
		section_new = regex.sub('#OrangeWhiteOrange',
								'#GreenWhiteGreen', section_new)

	elif determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "pathogenic":
		section_new = regex.sub('#fc8d62', '#d42859', content)
		section_new = regex.sub('#OrangeWhiteOrange', '#RedWhiteRed', section_new)

	elif determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "unknown":
		section_new = regex.sub('#fc8d62', '#9c9c9c', content)
		section_new = regex.sub('#OrangeWhiteOrange', '#GrayWhiteGray', section_new)

	elif determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "intermediate":
		section_new = content

	else:
		section_new = regex.sub('#fc8d62', '#b8b8b8', content)
		section_new = regex.sub('#OrangeWhiteOrange', '#GrayWhiteGray', section_new)

	return section_new


def replaceSVGgradients(content):
	"""Replacing gradients with solid colours in SVG file. This is needed due to the lack of gradients support in SVGlib

	Args:
		content (str): SVG file content

	Returns:
		str: SVG file content after replacing gradients with solid colours
	"""

	replaced = regex.sub(r'url\(#GreenWhiteGreen\)', '#d3f2c7', content)
	replaced = regex.sub(r'url\(#OrangeWhiteOrange\)', '#ffe3bd', replaced)
	replaced = regex.sub(r'url\(#GrayWhiteGray\)', '#e6e6e6', replaced)
	replaced = regex.sub(r'url\(#RedWhiteRed\)', '#ffdede', replaced)
	replaced = regex.sub(r'url\(#BlueWhiteBlue\)', '#deeefa', replaced)

	return replaced


def getSVGContent(allele1_results, allele2_results, file_svg, normal_range, intermediate_range, path_range_min):
	"""Get the SVG's content and modify this so the colours would correspond to the allele length (either red for pathogenic, green for normal, orange for intermediate and grey for unknown range)

	Args:
		allele1_results (array): Allele 1 length and CI
		allele2_results (array): Allele 2 length and CI
		file_svg (str): SVG file path
		normal_range (dict): Minimum and maximum value of the normal range
		intermediate_range (dict): Minimum and maximum value of the intermediate range
		path_range_min (int): Pathogenic cut-off value

	Returns:
		str: Final SVG file content after colourisation
	"""
	
	replace_svg_to_code = '<svg id="svg" '
	replace_defs_to_code = """<defs>
			<linearGradient id="GrayWhiteGray" x1="0%" y1="0%" x2="0%" y2="100%">
			<stop offset="0%" style="stop-color:#9c9c9c;stop-opacity:0.8" />
			<stop offset="50%" style="stop-color:#9c9c9c;stop-opacity:0.1" />
			<stop offset="100%" style="stop-color:#9c9c9c;stop-opacity:0.8" />
			</linearGradient>
			<linearGradient id="RedWhiteRed" x1="0%" y1="0%" x2="0%" y2="100%">
			<stop offset="0%" style="stop-color:#ed809f;stop-opacity:0.8" />
			<stop offset="50%" style="stop-color:#ed809f;stop-opacity:0.1" />
			<stop offset="100%" style="stop-color:#ed809f;stop-opacity:0.8" />
			</linearGradient>"""

	content = open(file_svg, 'r').read()
	output_html = ""
	matches = regex.findall(r'(.*?marker-end=\"url\(#arrow\)\" />)(.*)(marker-start.*?</svg>)', content, regex.DOTALL)
	
	if matches:
		tmp_rgx = matches[0][0].replace('<svg ', replace_svg_to_code)
		output_html += tmp_rgx.replace("<defs>", replace_defs_to_code)
		replaceA1colours = replaceSVGColours(matches[0][1], normal_range, intermediate_range, path_range_min)
		replaceA2colours = replaceSVGColours(matches[0][2], normal_range, intermediate_range, path_range_min)
		replaceA1units = regex.sub(r'(' + str(allele1_results[0]) + ' units)', r'\1 (CI ' + allele1_results[1] + ')', replaceA1colours)
		replaceA2units = regex.sub(r'(' + str(allele2_results[0]) + ' units)', r'\1 (CI ' + allele2_results[1] + ')', replaceA2colours)
		output_html += replaceA1units
		output_html += replaceA2units

	else:
		matches = regex.findall(r'(.*?marker-end=\"url\(#arrow\)\" />)(.*)(</svg>)', content, regex.DOTALL)
		tmp_rgx = matches[0][0].replace('<svg ', replace_svg_to_code)
		output_html += tmp_rgx.replace("<defs>", replace_defs_to_code)
		replaceA1colours = replaceSVGColours(matches[0][1], normal_range, intermediate_range, path_range_min)
		output_html += regex.sub(r'(' + str(allele1_results[0]) + ' units)', r'\1 (CI ' + allele1_results[1] + ')', replaceA1colours)
		output_html += matches[0][2]

	return output_html


def extractDataFromJSON(file_json, locus_name):
	"""Open the ExpansionHunter's JSON results file and get the content

	Args:
		file_json (str): ExpansionHunter's JSON results file path
		locus_name (str): Locus name

	Returns:
		dict: LocusResults section in the JSON file which contains the genotyping information
	"""

	with open(file_json, 'r') as f:
		content = json.load(f)
		call_results = content['LocusResults'][locus_name]

	return call_results


def extractDataFromVCF(file_vcf, locus_name):
	"""Open the ExpansionHunter's VCF results file and get the total number of reads consistent with the genotype

	Args:
		file_vcf (str): ExpansionHunter's VCF results file path
		locus_name (str): Locus name

	Returns:
		dict: Number of different types of reads that are in consistent with the estimated genotype
	"""

	call_results = {}

	with open(file_vcf, 'r') as f:
		lines = f.readlines()
		for line in lines:
			if line.startswith("#"):
				continue

			filter_status = line.split("\t")[6]
			locus = regex.search(r'VARID=(.*?);REPID', line).group(1)

			if locus != locus_name:
				continue

			total_reads_spanning = line.split("\t")[9].split(":")[4]
			total_reads_flanking = line.split("\t")[9].split(":")[5]
			total_reads_inrepeat = line.split("\t")[9].split(":")[6]

			call_results.update({
				"TotalOfSpanningReads": total_reads_spanning,
				"TotalOfFlankingReads": total_reads_flanking,
				"TotalOfInrepeatReads": total_reads_inrepeat,
				"FilterStatus": filter_status,
			})

	return call_results


def determinePopulationOutlier(allele_len, pop_repeats, pop_counts):
	"""Determines whether allele length is a population outlier

	Args:
		allele_len (int): Allele length in repeats
		pop_repeats (array): Population repeats
		pop_counts (array): Counts for population repeats

	Returns:
		bool: True if is a population outlier, otherwise False
	"""
	zscore_threshold = 3.719 # 1 in 10000 or P = .0001

	pop_repeats = [int(e) if e.isdigit() else e for e in pop_repeats.split(', ')]
	pop_counts = [int(e) if e.isdigit() else e for e in pop_counts.split(', ')]

	data = []
	for index, rep in enumerate(pop_repeats):
		for i in range(0, pop_counts[index]):
			data.append(rep)

	mean = np.mean(data)
	std = np.std(data)
	zscore = round(abs((allele_len-mean)/std), 3) if std > 0 else False

	is_outlier = True if abs(zscore) > zscore_threshold else False

	return is_outlier, zscore

def checkProgramParameter(program, parameter):
	"""Check if a command-line program accepts a specific parameter by invoking the program's help.

	This function runs the specified program with the '--help' argument to access its documentation, then checks if the specified parameter is part of the help text. It's a way to programmatically verify if a command-line tool supports a particular option without fully executing its primary function.

	Args:
		program (str): The name or path of the program to check. Must be executable from the current environment.
		parameter (str): The command-line parameter to look for in the program's help output.

	Returns:
		bool: True if the parameter is found in the program's help output, indicating the program likely accepts it. False if the parameter is not found or if an error occurs during execution.
	"""

	command = [program, '--help']
	
	try:
		result = subprocess.run(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, text = True, check = True)
		if parameter in result.stdout or parameter in result.stderr:
			return True
		else:
			return False
	except subprocess.CalledProcessError as e:
		return False
	except Exception as e:
		return False

def genotypeSample(config, disease_info, original_file_path, file_path, output_folder, locus_name, is_custom, sample_sex, genome, reference_file, variants, analysis_type, genome_has_chr, repeats_mates, repeats_flanking, fully_rep_read_sources, catalog_entry):
	"""Run ExpansionHunter on the analysis ready BAM file received

	Args:
		file_path (str): File path of the analysis ready BAM file
		file_uuid (str): Unique file name of the sample
		locus_name (str): Name of the targeted locus
		genome (str): Genome assembly
		variants (dict): Personalised variant catalogue
		genome_has_chr (bool): Does the genome used has 'chr' in chromosome names? True if yes, otherwise False

	Returns:
		dict: Results extracted out from ExpansionHunter output JSON and VCF files, in case of an error return error message
	"""
	with tempfile.TemporaryDirectory() as tempdir:
		# Setting stdout based on the value of 'verbose'
		stdout_setting = None if config["verbose"] else subprocess.DEVNULL

		file_name = os.path.basename(file_path)
		tmp_sample_name = os.path.splitext(file_name)[0]
		temp_file = os.path.join(tempdir, tmp_sample_name)
		
		# Create variant catalogue from regions file
		temp_variant_catalogue = os.path.join(tempdir, 'variant_catalog.json')

		with open(temp_variant_catalogue, 'w') as outfile:
			json.dump(variants, outfile, indent = 4)

		sample_sex_for_genotyping = sample_sex if sample_sex in ["male", "female"] else "female" # Use female as sample sex as default if not defined
		variant_id = variants["LocusId"]
		svg_base64 = None

		if variant_id == "XYLT1" and genome in ["hg38", "hg19"]:
			# If the locus is XYLT1 then realign it on a decoy chromosome for an accurate genotyping
			reference_file = os.path.join(scriptpath, "reference/XYLT1/xylt1_ref.fa")
			xylt1_mapped_file = tempdir+tmp_sample_name+".xylt1mapped.bam"

			temp_variant_catalogue = os.path.join(scriptpath, "reference/XYLT1/variant_catalog_extended.json") if analysis_type == "extended" else os.path.join(scriptpath, "reference/XYLT1/variant_catalog_standard.json")
			bam2fast(file_path, tempdir) # Convert to FASTQ

			# Realign on a new reference genome, sort and index
			bwaalign = subprocess.Popen([config["filepath_bwa"], "mem", "-aM", "-A", "1", "-B", "6", "-O", "4", reference_file, tempdir+tmp_sample_name+".regions_R1.fq", tempdir+tmp_sample_name+".regions_R2.fq"], stdout = subprocess.PIPE, stderr = subprocess.DEVNULL)
			samsort = subprocess.Popen([config["filepath_samtools"], "sort", "-"], stdin = bwaalign.stdout, stdout = subprocess.PIPE)
			samview = subprocess.Popen([config["filepath_samtools"], "view", "-Sb"], stdin = samsort.stdout, stdout = subprocess.PIPE)
			samviewoutput = samview.communicate()[0]

			with open(xylt1_mapped_file, 'wb') as outfile:
				outfile.write(samviewoutput)

			subprocess.call([config["filepath_samtools"], "index", xylt1_mapped_file])

			file_path = xylt1_mapped_file

		if config["verbose"]:
			print("-- 7 -- Genotyping " + variant_id + " with ExpansionHunter -- 7 --")

		# Initialize the command list with basic parameters
		eh_genotyping_command = [
			config["filepath_expansionhunter"],
			"--reads", file_path, 
			"--reference", reference_file,
			"--variant-catalog", temp_variant_catalogue, 
			"--output-prefix", temp_file,
			"--analysis-mode", config["eh_analysis_mode"],
			"--sex", sample_sex_for_genotyping
		]

		if int(config['min_no_reads_eh']) < 10:
			# Test if we can use --min-locus-coverage parameter with the version of ExpansionHunter in the system
			if checkProgramParameter(config["filepath_expansionhunter"], "--min-locus-coverage"):
				eh_genotyping_command += ["--min-locus-coverage", str(config["min_no_reads_eh"])]
			else:
				print("-- WARNING >>> The version of ExpansionHunter used does not accept the '--min-locus-coverage' parameter. Therefore, the minimum number of reads for ExpansionHunter will default to 10, regardless of the configuration parameter. <<< WARNING --" )

		if int(config['fregion_length_bp']) < 1000:
			# Test if we can use --min-locus-coverage parameter with the version of ExpansionHunter in the system
			if checkProgramParameter(config["filepath_expansionhunter"], "--region-extension-length"):
				eh_genotyping_command += ["--region-extension-length", str(config["fregion_length_bp"])]
			else:
				print("-- WARNING >>> The version of ExpansionHunter used does not accept the '--region-extension-length' parameter. Therefore, the region extension length for ExpansionHunter will default to 1000, regardless of the configuration parameter. <<< WARNING --" )

		# Call ExpansionHunter
		subprocess.call(eh_genotyping_command, stdout = stdout_setting)

		eh_vcf_file_path = temp_file + ".vcf"

		# Sort and index
		if config["verbose"]:
			print("-- 8 -- Sorting and indexing " + variant_id + " BAMlet with Samtools -- 8 --")

		subprocess.call([config["filepath_samtools"], "sort", "-o", temp_file + "_realigned-sorted.bam", temp_file + "_realigned.bam"], stdout = stdout_setting)
		subprocess.call([config["filepath_samtools"], "index", temp_file + "_realigned-sorted.bam"], stdout = stdout_setting)

		if config["verbose"]:
			print("-- 9 -- Creating read alignments for " + variant_id + " with REViewer -- 9 --")

		# Call REViewer
		reviewer_command = [
			config["filepath_reviewer"],
			"--reads", temp_file + "_realigned-sorted.bam",
			"--reference", reference_file, 
			"--catalog", temp_variant_catalogue, 
			"--vcf", eh_vcf_file_path,
			"--locus", variant_id,
			"--output-prefix", temp_file
		]

		if int(config['fregion_length_bp']) < 1000:
			# Test if we can use --min-locus-coverage parameter with the version of ExpansionHunter in the system
			if checkProgramParameter(config["filepath_reviewer"], "--region-extension-length"):
				reviewer_command += ["--region-extension-length", str(config["fregion_length_bp"])]
			else:
				print("-- WARNING >>> The version of REViewer used does not accept the '--region-extension-length' parameter. Therefore, the region extension length for REViewer will default to 1000, regardless of the configuration parameter. <<< WARNING --" )
		
		subprocess.call(reviewer_command, stdout = stdout_setting)

		if config["verbose"]:
			print("-- 10 -- Extracting data from " + variant_id + " analysis output files -- 10 --")

		extracted_results = extractDataFromJSON(temp_file + ".json", locus_name)  # Extract data from json file
		extracted_total_reads = extractDataFromVCF(temp_file + ".vcf", locus_name)  # Extract data from VCF file

		# Merge results from JSON and VCF file (total number of reads comes from VCF file)
		extracted_results['Variants'][locus_name].update(extracted_total_reads)

		# Determine whether analysing the locus ended up any results by looking at the count of returned reads, if zero then return an error message
		if extracted_results['Variants'][locus_name]["CountsOfFlankingReads"] == '()' and extracted_results['Variants'][locus_name]["CountsOfInrepeatReads"] == '()' and extracted_results['Variants'][locus_name]["CountsOfSpanningReads"] == '()':
			results_empty = {
				locus_name: {
					"TargetedLocus": {
						"LocusID": locus_name,
						"Coordinates": catalog_entry['LocationCoordinates'][genome],
						"Motif": catalog_entry["MotifPlusStrand"],
						"CorrespondingDisease": catalog_entry['Diseases'],
					},
					"Flag": -1,
					"Filter": "NoOrVeryLowCoverageAtSTRLocus"
				}
			}
			return results_empty # No read alignments
			
		else:
			# If there are more than one disease associated with the locus then pick the one which has the lowest pathogenic cut-off to compare alleles to
			all_locus_dis = {}

			for dis in catalog_entry['Diseases']:
				dis_path_cutoff = int(catalog_entry['Diseases'][dis]['PathogenicCutoff']) if catalog_entry['Locus'] != 'FMR1' else int(catalog_entry['Diseases'][dis]['PathogenicCutoff'].split("-")[0])  # FMR1 fix
				all_locus_dis.update({dis: dis_path_cutoff})
			analysed_disease = min(all_locus_dis, key=all_locus_dis.get)

			# Otherwise put results together before sending them back to the client
			normal_range = catalog_entry['Diseases'][analysed_disease]['NormalRange']
			path_range_min = int(catalog_entry['Diseases'][analysed_disease]['PathogenicCutoff']) if catalog_entry['Locus'] != 'FMR1' else int(catalog_entry['Diseases'][analysed_disease]['PathogenicCutoff'].split("-")[0])  # FMR1 fix
			intermediate_range = catalog_entry['Diseases'][analysed_disease]['IntermediateRange']

			no_alleles = 0

			if '/' in extracted_results['Variants'][locus_name]['Genotype']:
				no_alleles = 2
				allele1_len, allele2_len = alleleSplitToNumArray(extracted_results['Variants'][locus_name]['Genotype'], '/')
				allele1_ci_range, allele2_ci_range = extracted_results['Variants'][locus_name]['GenotypeConfidenceInterval'].split('/')
			else:
				no_alleles = 1
				allele1_len, allele2_len = int(extracted_results['Variants'][locus_name]['Genotype']), 0
				allele1_ci_range, allele2_ci_range = extracted_results['Variants'][locus_name]['GenotypeConfidenceInterval'], "0-0"

			if is_custom:
				is_population_outlier_a1, is_population_outlier_a2 = False, False
				population_zscore_a1, population_zscore_a2 = 0, 0
			else:
				# Determine whether any of the alleles are population outliers
				if catalog_entry["PopulationData"]:
					is_population_outlier_a1, population_zscore_a1 = determinePopulationOutlier(allele1_len, catalog_entry["PopulationData"]["All"]["Repeats"], catalog_entry["PopulationData"]["All"]["Counts"])
					is_population_outlier_a2, population_zscore_a2 = determinePopulationOutlier(allele2_len, catalog_entry["PopulationData"]["All"]["Repeats"], catalog_entry["PopulationData"]["All"]["Counts"]) if no_alleles == 2 else False, 0
				else:
					is_population_outlier_a1, is_population_outlier_a2 = None, None
					population_zscore_a1, population_zscore_a2 = None, None

			call_fragment_length = extracted_results['FragmentLength']
			call_read_length = extracted_results['ReadLength']

			call_coverage = round(extracted_results['Coverage'], 1)

			if no_alleles == 1:
				total_reads_spanning = [int(extracted_results['Variants'][locus_name]['TotalOfSpanningReads'])]
				total_reads_flanking = [int(extracted_results['Variants'][locus_name]['TotalOfFlankingReads'])]
				total_reads_inrepeat = [int(extracted_results['Variants'][locus_name]['TotalOfInrepeatReads'])]
			elif no_alleles == 2:
				total_reads_spanning = alleleSplitToNumArray(extracted_results['Variants'][locus_name]['TotalOfSpanningReads'], '/')
				total_reads_flanking = alleleSplitToNumArray(extracted_results['Variants'][locus_name]['TotalOfFlankingReads'], '/')
				total_reads_inrepeat = alleleSplitToNumArray(extracted_results['Variants'][locus_name]['TotalOfInrepeatReads'], '/')

			filter_status = extracted_results['Variants'][locus_name]['FilterStatus']

			if (no_alleles == 1 and allele1_len > 0) or (no_alleles == 2 and allele1_len > 0 and allele2_len > 0):
				svg_file_path = temp_file + '.' + locus_name + '.svg'
				if os.access(svg_file_path, os.R_OK):
					reads_svg_content = getSVGContent([allele1_len, allele1_ci_range], [allele2_len, allele2_ci_range], svg_file_path, normal_range, intermediate_range, path_range_min)
					reads_svg_content_gradients_replaced = replaceSVGgradients(reads_svg_content)  # Replace gradients to solid colours
					svg_base64 = base64.b64encode(bytes(reads_svg_content_gradients_replaced, 'utf-8')).decode()
		
		if config["verbose"]:
			print("-- 11 -- Getting repeat ranges for " + variant_id + " -- 11 --")

		diagnose_allele1 = determineDiagnose(allele1_len, normal_range, intermediate_range, path_range_min)
		diagnose_allele2 = determineDiagnose(allele2_len, normal_range, intermediate_range, path_range_min)

		if config["verbose"]:
			print("-- 12 -- Getting the population data for " + variant_id + ", if present -- 12 --")

		if not is_custom and catalog_entry["PopulationData"]:
			population_repeats = [int(e) if e.isdigit() else e for e in catalog_entry["PopulationData"]["All"]["Repeats"].split(', ')]
			is_population_outlier_a1, population_zscore_a1 = determinePopulationOutlier(allele1_len, catalog_entry["PopulationData"]["All"]["Repeats"], catalog_entry["PopulationData"]["All"]["Counts"]) 
			is_population_outlier_a2, population_zscore_a2 = determinePopulationOutlier(allele2_len, catalog_entry["PopulationData"]["All"]["Repeats"], catalog_entry["PopulationData"]["All"]["Counts"])

		# Give a flag
		flag = 0 # Default is normal range
		flag_output = 0 # Flag that is used to decide in output

		total_reads_with_path_motif = sum(repeats_flanking.values()) if repeats_flanking else 0 # Calculated by STRipy

		if not is_custom:
			if diagnose_allele1 == "unknown" or diagnose_allele2 == "unknown":
				if (is_population_outlier_a1 == True and allele1_len > normal_range["Max"]) or (is_population_outlier_a2 == True and allele2_len > normal_range["Max"]): # Only if expanded above normal range (not contracted)
					flag = 1
					flag_output = 2
				else:
					flag = 1
					flag_output = 1

			if (diagnose_allele1 == "unknown" and catalog_entry["RepeatType"] == "VNTR") or (diagnose_allele2 == "unknown" and catalog_entry["RepeatType"] == "VNTR"):
				flag = 1
				flag_output = 1

		if diagnose_allele1 == "normal" or diagnose_allele2 == "normal":
			if (is_population_outlier_a1 == True) or (is_population_outlier_a2 == True):
				flag = 0
				flag_output = 1
			else:
				flag = 0
				flag_output = 0

		if diagnose_allele1 == "intermediate" or diagnose_allele2 == "intermediate":
			flag = 2
			flag_output = 2

		if diagnose_allele1 == "pathogenic" or diagnose_allele2 == "pathogenic":
			if "D" in catalog_entry['Diseases'][analysed_disease]["Inheritance"] and (diagnose_allele1 == "pathogenic" or diagnose_allele2 == "pathogenic"): # If Dominant and at least one allele in pathogenic range
				flag = 3
				flag_output = 3
			elif "R" in catalog_entry['Diseases'][analysed_disease]["Inheritance"] and (diagnose_allele1 == "pathogenic" and diagnose_allele2 == "pathogenic"): # If recessive and both alleles in pathogenic range
				flag = 3
				flag_output = 3
			elif catalog_entry['Diseases'][analysed_disease]["Inheritance"] == "NI" and (diagnose_allele1 == "pathogenic" or diagnose_allele2 == "pathogenic"): # If not inherited
				flag = 3
				flag_output = 3
			else:
				flag = 1
				flag_output = 1

		# Filter results based on the minimum number of reads required
		if not is_custom and locus_name != "XYLT1":
			if total_reads_with_path_motif < config['min_no_reads_stripy']:
				filter_status = f"LowNumberReadsWithPathogenicMotif (<{config['min_no_reads_stripy']})"

		if config['filter_likely_false_gcn'] and locus_name in ["ARX_1", "ARX_2", "HOXA13_1", "HOXA13_2", "HOXA13_3"] and total_reads_spanning[-1] == 0:
			# If it is ARX or HOXA13 locus which is known to frequently provide false positives (second allele expanded to around 70 repeats) then check the second allelele and if it has no spanning reads then filter it out
			filter_status = f"PotentiallyFalsePositive (expansion over the read length)"

		if (total_reads_spanning[0] + total_reads_flanking[0] + total_reads_inrepeat[0]) < config['min_no_reads_eh']:
			if no_alleles == 1 or (no_alleles == 2 and total_reads_spanning[1] + total_reads_flanking[1] + total_reads_inrepeat[1]) < config['min_no_reads_eh']:
				filter_status = f"LowNumberReads (<{config['min_no_reads_eh']})"

		if allele1_len == 0 and allele2_len == 0:
			filter_status = 'BothAllelesZero'
		
		if catalog_entry['RepeatType'] == "ImperfectGCN" and total_reads_spanning[0] == 0:
			if no_alleles == 1 or (no_alleles == 2 and total_reads_spanning[1] == 0):
				filter_status = "NoSpanningReadsFoundGCN"

		if filter_status != "PASS":
			flag = -1

		if config["verbose"]:
			print("-- 13 -- Combining genotyping results of " + variant_id + " -- 13 --")

		results = {
			locus_name: {
				"TargetedLocus": {
					"LocusID": locus_name,
					"Motif": catalog_entry["MotifPlusStrand"],
					"Coordinates": catalog_entry['LocationCoordinates'][genome],
					"CorrespondingDisease": catalog_entry['Diseases'],
				},
				"Flag": flag,
				"Filter": filter_status
			}
		}

		if flag_output >= 0:
			results[locus_name].update({
				"Alleles": [
					{
						"Repeats": allele1_len,
						"CI": {
							"Min": int(allele1_ci_range.split("-")[0]),
							"Max": int(allele1_ci_range.split("-")[1])
						},
						"Range": diagnose_allele1,
						"IsPopulationOutlier": is_population_outlier_a1 if is_population_outlier_a1 is not None else "NA",
						"PopulationZscore": round(population_zscore_a1, 3) if population_zscore_a1 else "NA"
					}
				]
			})
			
			if no_alleles == 2:
				results[locus_name]["Alleles"].append({
						"Repeats": allele2_len,
						"CI": {
							"Min": int(allele2_ci_range.split("-")[0]),
							"Max": int(allele2_ci_range.split("-")[1])
						},
						"Range": diagnose_allele2,
						"IsPopulationOutlier": is_population_outlier_a2 if is_population_outlier_a2 is not None else "NA",
						"PopulationZscore": round(population_zscore_a2, 3) if population_zscore_a2 else "NA"
				})
			
			results[locus_name].update({
				"Metadata": {
					"Coverage": round(call_coverage),
					"ReadLength": call_read_length,
					"FragmentLength": call_fragment_length,
					"TotalOfSpanningReads": total_reads_spanning,
					"TotalOfFlankingReads": total_reads_flanking,
					"TotalOfInrepeatReads": total_reads_inrepeat,
					"FilterStatus": filter_status,
					"MatesFullOfRepeats": repeats_mates,
					"HighestPathRepeatsInFlanking": repeats_flanking,
					"FullyRepeatedReadSources": fully_rep_read_sources
				}
			})

			if config["output_html"]:
				results[locus_name].update({
				"SVG": svg_base64 if flag_output >= config["output_svg_flag_threshold"] else None
			})

		return results


def analyseSampleFile(config, original_file_path, fileslist, req_output_folder, req_locus, is_custom, req_sex, req_genome, req_reference, req_analysis, req_genome_has_chr, repeats_mates, repeats_flanking, offtargets, fully_rep_read_sources, catalog_entry):
	"""Iniates the analysis process. Validating input variables, creating variant catalogue for ExpansionHunter and running the tool, extracting our results and using REViewer to get read alignments, which will be then colourised, followed by generation of PDF report

	Returns:
		json: Response in JSON format for the Client
	"""

	# Convert string to integer if a numeric value exists
	repeats_mates = int(repeats_mates) if repeats_mates != '' else ''

	# Check for input errors
	error_message = ''
	if not req_locus and not req_genome:
		error_message = "Empty input"

	disease_info = catalog_entry

	if is_custom:
		targeted_locus_name = req_locus["LocusID"]
		targeted_motif = req_locus["Motif"]
		targeted_coordinates = req_locus["Coordinates"]
	else:
		# Get information of the disease/gene that the user wants to analyse
		if disease_info == False:
			error_message = "Disease not supported"

		if req_genome not in config['supported_genomes']:
			error_message = "Genome not supported"

		# In case of an error message defined, return it to user and do not continue with analysis
		if error_message:
			return json.dumps({"Error": True, "ErrorMessage": error_message})

		# Create a personalised variant catalogue for the sample
		disease_coord = disease_info['LocationCoordinates'][req_genome]
		if not req_genome_has_chr:
			disease_coord = disease_coord.replace("chr", "")

		# Set variables for variant catalog
		targeted_locus_name = disease_info['Locus']
		targeted_motif = disease_info["MotifPlusStrand"]
		targeted_coordinates = disease_coord

	tmp_bamfile_path = fileslist[0][1]
	tmp_filename = tmp_bamfile_path.split(".")[-2].split("/")[-1]

	variant = {
		"LocusId": targeted_locus_name,
		"LocusStructure": "(" + targeted_motif + ")*",
		"ReferenceRegion": targeted_coordinates,
		"VariantType": "RareRepeat" if offtargets else "Repeat"
	}

	# Append detected off-target regions (where reads are actually aligned in that sample) to the catalogue when off-target regions were received (i.e. analysis type is extended)
	if offtargets:
		variant.update({
			"OfftargetRegions": json.loads(offtargets)
		})

	if config["verbose"]:
		print("-- 6 -- Using the variant specification: " + str(variant) + " for " + targeted_locus_name + " -- 6 --")

	response = genotypeSample(config, disease_info, original_file_path, tmp_bamfile_path, req_output_folder, targeted_locus_name, is_custom, req_sex, req_genome, req_reference, variant, req_analysis, req_genome_has_chr, repeats_mates, repeats_flanking, fully_rep_read_sources, catalog_entry)

	return response

def loadConfiguration(config_file):
	with open(config_file, 'r') as f:
		config = json.load(f)
	return config


def getConfiguration():
	"""Provides configuration options to the app which can be used to change under settings

	Returns:
		dict: Configuration
	"""
	return loadConfiguration()



def estimateReadFragSize(bamfile, reference, alignments = 1000):
	"""Estimate the median size of reads and fragments

	Args:
		bamfile (file): BAM file
		reference (str): Path to reference genome (FASTA file)
		alignments (int, optional): How many alignments to iterate through. Defaults to 1000.

	Returns:
		int: Median read length
		int: Median fragment length
	"""

	samfile = pysam.AlignmentFile(bamfile, 'rb', reference_filename=reference)

	inserts = np.array([read.tlen for read in samfile.head(alignments) if read.tlen > 0])
	reads = np.array([read.query_length for read in samfile.head(alignments)])

	frag_median = int(np.median(inserts)) if inserts.size > 0 else 0
	read_median = int(np.median(reads)) if reads.size > 0 else 0

	return read_median, frag_median


def determineRegionOverlap(reference, offtarget):
	"""Determine whether one region overlaps the other (such as the reference region and one of the off-target ones)

	Args:
		reference (array): Coordinates of the reference's start (int) and end position (int)
		offtarget (array): Coordinates of the off-target start (int) and end position (int)

	Returns:
		bool: Returns True if two regions overlap and False if not
	"""

	overlap = max(0, min(reference[2], offtarget[2]) - max(reference[1], offtarget[1]))

	if reference[0] == offtarget[0] and overlap > 0:
		return True
	else:
		return False


def mergeOfftargets(config, offtargets_list):
	"""Merge overlapping (off-target) regions

	Args:
		offtargets_list (array): List of off-target regions to be merged

	Returns:
		array: List of merged overlapping off-target regions
	"""

	offtargets = []
	offtargets_list = [i for i in offtargets_list if i[0]]

	# Exclude alternate contigs if set True in config
	if config["use_alt_contigs"] == False:
		temp_ot_contigs = [i[0] for i in offtargets_list if "_" not in i[0] and "HLA" not in i[0]]
	else:
		temp_ot_contigs = [i[0] for i in offtargets_list]

	temp_ot_contigs = list(set(temp_ot_contigs))

	for contig in temp_ot_contigs:
		list_ot = []
		for pos in offtargets_list:
			if pos[0] == contig:
				pos1_positive = pos[1] if pos[1] > 0 else 0 # Also check that start position is not negative, if it is then set the start position as zero
				list_ot.append([pos1_positive, pos[2]])

		list_ot.sort(key = lambda interval: interval[0])
		merged = [list_ot[0]]

		for current in list_ot:
			previous = merged[-1]
			if current[0] <= previous[1]:
				previous[1] = max(previous[1], current[1])
			else:
				merged.append(current)

		for region in merged:
			offtargets.append(str(contig) + ':' + str(region[0]) + '-' + str(region[1]))

	return offtargets


def convertToComplementarySequence(seq):
	"""Create reverse complementary DNA sequence

	Args:
		seq (str): DNA sequence

	Returns:
		str: Reverse complementary DNA sequence
	"""

	seq = seq[::-1]
	new_seq = ''

	for nucleotide in seq:
		if nucleotide == 'A':
			new_seq += 'T'
		elif nucleotide == 'T':
			new_seq += 'A'
		elif nucleotide == 'G':
			new_seq += 'C'
		elif nucleotide == 'C':
			new_seq += 'G'

	return new_seq


def calculateRepeatedReads(motif, sequence, rep_type):
	"""Calculate the number of repeats in a sequence

	Args:
		motif (str): Repeat unit
		sequence (str): DNA (read) sequence
		rep_type (str): Repeat type (Standard, Imperfect GCN or Replaced/Nested)

	Returns:
		int: Number of repeat unit matches in the sequence
	"""

	# If Imperfect GCN then replace the N with selection of nucleotides used in the formula for matching motifs with regex
	if rep_type == "ImperfectGCN":
		imperfect_motif = motif.replace("N", "[A|T|G|C]")
		motif_to_find = "(" + imperfect_motif + ")"
	else:
		motif_to_find = motif

	matches = len(regex.findall(motif_to_find, sequence))

	return matches


def matchSequence(section, whole):
	"""Determine whether one sequence (flanking end) matches with another sequence (a whole read)

	Args:
		section (str): DNA sequence to match (flanking end)
		whole (str): Whole DNA sequence (read) where to look the match

	Returns:
		(match object): Regex match object
	"""

	# Allow maximum of 1 insertion, 1 deletion, 1 substitution and 1 error in total
	return regex.search("(" + section + "){i<=1,d<=1,s<=1,e<=1}", whole)


def isFullyRepeatedRead(config, read_seq, motif_pathogenic, read_length):
	"""Determines whether read sequence is made solely of repeats or not. Allowing one insertion/deletion and two substitutions in the sequence.

	Args:
		read_seq (str): DNA (read) sequence
		motif_pathogenic (str): Pathogenic repeat unit
		read_length (int): Median read length

	Returns:
		bool: True if read is fully made of repeats, otherwise False
	"""
	if len(read_seq) > read_length*config['min_req_seq_len_proportion']:
		longseq = (round(read_length/len(motif_pathogenic)) * motif_pathogenic) + motif_pathogenic*2 # Create a long sequence which would be in the length of the average read plus two additional motifs
		longseq_comp = convertToComplementarySequence(longseq)

		# Allow max one insertion or deletion and max 3 substitution, but no more than 3 in total
		search_in_longseq = regex.search("(" + read_seq + "){s<=3,i<=1,d<=1,e<=3}", longseq)
		search_in_longseq_comp = regex.search("(" + read_seq + "){s<=3,i<=1,d<=1,e<=3}", longseq_comp)

		return True if search_in_longseq or search_in_longseq_comp else False
	else:
		return False


def calculateRepeatsInFlankingReads(config, seq, flanking_before, flanking_after, motif_pathogenic, repeat_type, read_length):
	"""Calculates the number of repeats found in flanking reads

	Args:
		seq (str): DNA (read) sequence
		flanking_before (str): Flanking DNA sequence before the STR locus
		flanking_after (str): Flanking DNA sequence after the STR locus
		motif_pathogenic (str): Pathogenic repeat unit
		repeat_type (str): Repeat type (Standard, Imperfect GCN or Replaced/Nested)
		read_length (int): Median read length

	Returns:
		(int): Number of repeat unit matches in the flanking reads
	"""

	flanking_before_comp = convertToComplementarySequence(flanking_after)
	flanking_after_comp = convertToComplementarySequence(flanking_before)
	motif_pathogenic_comp = convertToComplementarySequence(motif_pathogenic)

	# Check the flanking sequence before and after the STR region and determine whether it matches with the reference one
	forward = True if matchSequence(flanking_before, seq) or matchSequence(flanking_after, seq) else False
	reverse = True if matchSequence(flanking_before_comp, seq) or matchSequence(flanking_after_comp, seq) else False

	# If sequence is on the opposite strand then switch and flip over the flanking ends
	if reverse:
		flanking_before = flanking_before_comp
		flanking_after = flanking_after_comp
		motif_pathogenic = motif_pathogenic_comp

	if forward or reverse:
		seq_flanking_before = matchSequence(flanking_before, seq)
		seq_flanking_after = matchSequence(flanking_after, seq)

		str_start = seq_flanking_before.end() if seq_flanking_before else 0
		str_end = seq_flanking_after.start()+1 if seq_flanking_after else len(seq)

		if str_start > 0 and str_end < len(seq): # Selecting only spanning reads
			return "spanning", calculateRepeatedReads(motif_pathogenic, seq[str_start:str_end], repeat_type)
		else: # Selecting only flanking reads
			# Check whether the read's sequence length is least X of the median read length (e.g. at least 135 bp in case of 150 bp reads, including the flanking start region)
			# Secondly check whether the repeated region is at least Y of the read's length
			if len(seq) > read_length*config['min_req_seq_len_proportion'] and str_start < read_length*config['min_req_str_locus_proportion']:
				return "flanking", calculateRepeatedReads(motif_pathogenic, seq[str_start:str_end], repeat_type)
			else:
				return False, False
	else:
		return False, False


def valid_read(read):
	"""Check if a read is properly mapped

	Args:
		read (array): Read array

	Returns:
		bool: True if read has good mapping quality, otherwise False
	"""
	if read.mapping_quality >= 40 and read.reference_end and read.reference_start is not None:
		return True
	else:
		return False

def checkChromNames(input_file, reference):
	"""
	Search contigs that include chromosome names and then determine whether the genome has "chr" in its contigs names.

	Args:
	input_file: Input file name

	Returns:
	genome_has_chr: A boolean indicating if the genome contig names include "chr".
	"""

	contig_found = False
	contig_id = 0
	genome_has_chr = None

	input_file_read = pysam.AlignmentFile(input_file, 'rb', reference_filename=reference)

	while not contig_found:
		contig_name = input_file_read.get_reference_name(contig_id)

		if bool(regex.search(r'^chr\d{1,2}$|^chrX$|^chrY$|^\d{1,2}[^\w\d]$|^X$|^Y$', contig_name, regex.MULTILINE)):
			contig_found = True

			if "chr" not in contig_name:
				genome_has_chr = False
			else:
				genome_has_chr = True
		else:
			contig_id += 1

	return genome_has_chr




def findOfftargets(seq_file, reference, genome_has_chr, genome, config, disease_data):
	"""
	Processes genomic data to determine off-target regions based on the input parameters.

	Args:
	seq_file (str): Path to the sequencing file.
	reference (str): Path to the reference file.
	genome_has_chr (bool): Indicates if the genome contig names include "chr".
	genome (str): Genome version (e.g., 'hg38', 'hg19').
	config (dict): Configuration dictionary containing parameters like 'fregion_length_bp', 'ot_edges_bp'.
	disease_data (dict): Dictionary containing disease data including 'LocationCoordinates'.

	Returns:
	list: List of off-target regions.
	"""
	input_file = pysam.AlignmentFile(seq_file, 'rb', reference_filename=reference)
	chrom, start, end = regex.split(':|-', disease_data['LocationCoordinates'][genome]) # Reference coordinates

	if not genome_has_chr:
		chrom = chrom.replace("chr", "")

	section_min_start_pos = int(start) - config['fregion_length_bp']
	section_max_start_pos = int(end) + config['fregion_length_bp']

	offtargets = []
	offtargets_list = []
	mate_positions = []  # Genomic locations of mates

	process_read1_data = False
	read_length = 100  # Assuming read length, you might want to adjust this

	read_names = []
	for read in input_file.fetch(chrom, section_min_start_pos, section_max_start_pos):
		if read.is_read1 or read.is_read2:
			process_read1_data = True

		if process_read1_data:
			read_names.append(read.query_name)
			mate_positions.append([read.next_reference_name, read.next_reference_start, read.next_reference_start + read_length])

	if disease_data['Locus'] == "XYLT1":
		if genome == "hg38":
			mate_positions = [
				['chr11', 120511817, 120512260],
				['chr7', 100694219, 100694486],
				['chr1', 208244204, 208244434],
				['chr17', 66302232, 66302477],
				['chr4', 2059258, 2059558],
				['chrX', 16946643, 16946876],
				['chr2', 86914191, 86914491],
				['chr16_KZ559113v1_fix', 470000, 476000]
			]

		if genome == "hg19":
			mate_positions = [
				['chr16', 17561764, 17567779],
				['chr11', 120382526, 120382969],
				['chr7', 100291842, 100292109],
				['chr1', 208417549, 208417779],
				['chr17', 64298350, 64298595],
				['chr4', 2060985, 2061285],
				['chrX', 16964766, 16964999],
				['chr2', 87141314, 87141614]
			]

	merged_mate_positions = mergeOfftargets(config, mate_positions)
	merged_mate_positions.sort()

	for read in input_file.fetch(chrom, section_min_start_pos, section_max_start_pos):
		if not read.mate_is_unmapped:
			ot1_chrom = read.next_reference_name
			ot1_start = int(read.next_reference_start) - config['ot_edges_bp']
			ot1_end = ot1_start + read_length + (config['ot_edges_bp'] * 2)

			if determineRegionOverlap([chrom, section_min_start_pos, section_max_start_pos], [ot1_chrom, ot1_start, ot1_end]):
				continue

			offtargets_list.append([ot1_chrom, ot1_start, ot1_end])

	if offtargets_list:
		offtargets = mergeOfftargets(config, offtargets_list)

	offtargets.sort()

	# Count depth for each offtarget region and filter out large if required
	offtargets_final = []
	for region in offtargets:
		chrom, start, end = regex.split(':|-', region)

		region_coverage = input_file.count_coverage(chrom, int(start), int(end), quality_threshold=0)
		coverage_sum = np.mean(np.array([region_coverage[0], region_coverage[1], region_coverage[2], region_coverage[3]]))
		
		if coverage_sum < config["ea_max_depth_offtarget"]:
			offtargets_final.append(str(chrom) + ':' + str(start) + '-' + str(end))

	return offtargets_final

# Function to check if two regions overlap
def regions_overlap(start1, end1, start2, end2):
	return max(start1, start2) <= min(end1, end2)



def makeBAM(config, seq_file, analysis_type, genome, genome_has_chr, tmpbam, disease_data, reference):
	"""Create the analysis ready BAM file in a temporary folder and extract all reads relevant to the STR locus and determine off-target regions

	Args:
		seq_file (file): Original sample's BAM or CRAM sequence file
		analysis_type (str): Type of the analysis - either 'standard' or 'extended'
		genome (str): Detected or specified genome assembly
		tmpbam (file): Analysis ready BAM file in a temporary folder
		disease_data (dict): Variant information about the locus being targeted
		reference (str): Path to reference genome (FASTA file)

	Returns:
		file: Analysis ready BAM file
		str: Highest number of repeats found in flanking with the count
		int: Number of fully repeated mates
		array: List of off-target regions in case of Extended analysis, otherwise returns an empty array
		str: Detected or specified genome assembly
		bool: Returns True if genome uses 'chr' in their chromosome names, otherwise False
	"""

	input_file = pysam.AlignmentFile(seq_file, 'rb', reference_filename=reference)
	output_file = pysam.AlignmentFile(tmpbam, 'wb', template = input_file)

	# Work on the data now
	chrom, start, end = regex.split(':|-', disease_data['LocationCoordinates'][genome]) # Reference coordinates

	if not genome_has_chr:
		chrom = chrom.replace("chr", "")

	# Define the start and end positions of the whole reference section that is extracted out
	section_min_start_pos = int(start) - config['fregion_length_bp']
	section_max_start_pos = int(end) + config['fregion_length_bp']

	# Define the start and end positions for fragments where one read would fall into the STR reference region 
	read_length, fragment_size = estimateReadFragSize(seq_file, reference)
	fragment_min_start_pos = int(start) - (fragment_size-read_length)
	fragment_max_end_pos = int(end) + (fragment_size-read_length)

	path_repeats_in_spanning_reads = [] # Number of pathogenic repeats in spanning reads
	path_repeats_in_flanking_reads = [] # Number of pathogenic repeats in flanking reads
	path_repeats_in_mate_reads = [] # Number of pathogenic repeats in mates

	motif_pathogenic = disease_data["MotifPlusStrand"]
	flanking_before = disease_data["FlankingSequences"]["Before"]
	flanking_after = disease_data["FlankingSequences"]["After"]
	repeat_type = disease_data["RepeatType"]
	locus = disease_data["Locus"]

	# Check whether there are any exceptions for hg19 or hs1 genome and if yes then replace values
	if genome == "hs1" and "hs1" in disease_data["GenomeExceptions"]:
		if "FlankingSequences" in disease_data["GenomeExceptions"]["hs1"]:
			flanking_before = disease_data["GenomeExceptions"]["hs1"]["FlankingSequences"]["Before"]
			flanking_after = disease_data["GenomeExceptions"]["hs1"]["FlankingSequences"]["After"]

	if genome == "hg19" and "hg19" in disease_data["GenomeExceptions"]:
		if "FlankingSequences" in disease_data["GenomeExceptions"]["hg19"]:
			flanking_before = disease_data["GenomeExceptions"]["hg19"]["FlankingSequences"]["Before"]
			flanking_after = disease_data["GenomeExceptions"]["hg19"]["FlankingSequences"]["After"]

		if "MotifPlusStrand" in disease_data["GenomeExceptions"]["hg19"]:
			motif_pathogenic = disease_data["GenomeExceptions"]["hg19"]["MotifPlusStrand"]

	# Get the coverage
	cov_flanking_size = 300 # bp
	cov_start_pos = int(start) - cov_flanking_size 
	cov_end_pos = int(end) + cov_flanking_size

	region_coverage = input_file.count_coverage(chrom, cov_start_pos, cov_end_pos, quality_threshold = 15) # Set min quality score 15

	coverage_combined = np.array([region_coverage[0], region_coverage[1], region_coverage[2], region_coverage[3]]) # All nucleotides added together
	coverage_combined_sum = coverage_combined.sum(axis = 0).tolist()
	coverage_combined_sum = ', '.join([str(elem) for elem in coverage_combined_sum])

	# Get relevant reads to the locus
	process_read1_data = False


	# Use a set to keep track of unique read identifiers
	included_read_identifiers = set()

	# Process primary reads
	for read in input_file.fetch(chrom, section_min_start_pos, section_max_start_pos):
		if read.is_read1 or read.is_read2:
			process_read1_data = True
			
			# Create a unique identifier for the read
			read_identifier = read.query_name + "_" + str(read.query_sequence) + "_isSup" + str(read.is_supplementary)
			
			# Check if the read has already been included
			if read_identifier not in included_read_identifiers:
				output_file.write(read)
				included_read_identifiers.add(read_identifier)

				if process_read1_data:
					read_type, read_repeats = calculateRepeatsInFlankingReads(config, read.query_sequence, flanking_before, flanking_after, motif_pathogenic, repeat_type, read_length)
					if read_repeats:
						if read_type == "spanning":
							path_repeats_in_spanning_reads.append(read_repeats)
						elif read_type == "flanking":
							path_repeats_in_flanking_reads.append(read_repeats)

	# Find offtargets for extended analysis
	if analysis_type == "extended":
		offtargets_list = findOfftargets(seq_file, reference, genome_has_chr, genome, config, disease_data)
	else:
		offtargets_list = []


	# List to store read query names and their fully repeated status
	read_status_list = []
	# Dictionary to store counts of chromosome and start position pairs
	chrom_start_counts = defaultdict(list)

	# Process primary reads
	for read in input_file.fetch(chrom, section_min_start_pos, section_max_start_pos):
		if read.is_read1 or read.is_read2:
			process_read1_data = True
			
			# Create a unique identifier for the read
			read_identifier = read.query_name + "_" + str(read.query_sequence) + "_isSup" + str(read.is_supplementary)
			
			# Check if the read has already been included
			if read_identifier not in included_read_identifiers:
				read_is_fully_repeated = isFullyRepeatedRead(config, read.query_sequence, motif_pathogenic, read_length)
				read_status_list.append((read.query_name, read_is_fully_repeated, read.next_reference_name, read.next_reference_start))

				if process_read1_data:
					read_type, read_repeats = calculateRepeatsInFlankingReads(config, read.query_sequence, flanking_before, flanking_after, motif_pathogenic, repeat_type, read_length)
					if read_repeats:
						if read_type == "spanning":
							path_repeats_in_spanning_reads.append(read_repeats)
						elif read_type == "flanking":
							path_repeats_in_flanking_reads.append(read_repeats)

	# Process off-target regions
	for region in offtargets_list:
		ot_chrom, ot_coords = region.rsplit(':', 1)
		ot_start, ot_end = map(int, ot_coords.split('-'))

		for ot_read in input_file.fetch(ot_chrom, ot_start, ot_end):
			# Create a unique identifier for the off-target read
			ot_read_identifier = ot_read.query_name + "_" + str(ot_read.query_sequence) + "_isSup" + str(ot_read.is_supplementary)

			# Check if the off-target read has already been included
			if ot_read_identifier not in included_read_identifiers:
				ot_read_is_fully_repeated = isFullyRepeatedRead(config, ot_read.query_sequence, motif_pathogenic, read_length)
				read_status_list.append((ot_read.query_name, ot_read_is_fully_repeated, ot_read.next_reference_name, ot_read.next_reference_start))
				
				output_file.write(ot_read)
				included_read_identifiers.add(ot_read_identifier)

	# Process the read status list to filter unique query names
	unique_reads = {}
	for query_name, is_fully_repeated, chrom, start in read_status_list:
		if query_name in unique_reads:
			if unique_reads[query_name] != is_fully_repeated:
				unique_reads[query_name] = False
		else:
			unique_reads[query_name] = is_fully_repeated

	# Count only those reads where one read is fully repeated and the other is not
	for query_name, is_fully_repeated in unique_reads.items():
		if is_fully_repeated:
			for entry in read_status_list:
				if entry[0] == query_name:
					chrom, start, mate_chrom, mate_start = entry[2], entry[3], entry[2], entry[3]
					for region in offtargets_list:
						ot_chrom, ot_coords = region.rsplit(':', 1)
						ot_start, ot_end = map(int, ot_coords.split('-'))
						if mate_chrom == ot_chrom and (ot_start - 500 <= mate_start <= ot_end + 500):
							break
					else:
						chrom_start_counts[chrom].append(start)

	# Merge regions within 300 base pairs and count the number of reads per region
	merged_regions_counts = defaultdict(int)
	
	for chrom, starts in chrom_start_counts.items():
		starts.sort()
		merged_regions = []
		current_region = [starts[0], starts[0]]

		for start in starts[1:]:
			if start - current_region[1] <= 300:
				current_region[1] = start
			else:
				merged_regions.append(tuple(current_region))
				current_region = [start, start]
		merged_regions.append(tuple(current_region))

		for region in merged_regions:
			region_key = (chrom, region[0], region[1])
			region_count = sum(1 for start in starts if region[0] <= start <= region[1])
			merged_regions_counts[region_key] = region_count

	# Sort and get the top 3 regions with the highest number of reads
	sorted_regions_counts = sorted(merged_regions_counts.items(), key=lambda item: item[1], reverse=True)
	top_regions_counts = sorted_regions_counts[:3]

	catalog = loadCatalog()
	fully_rep_read_sources = []

	for (chrom, start, end), count in top_regions_counts:
		locus_names = []
		for locus in catalog:
			loc_coords = locus["LocationCoordinates"].get(genome, "")
			if loc_coords:
				loc_chrom, loc_range = loc_coords.split(':')
				loc_start, loc_end = map(int, loc_range.split('-'))
				if chrom == loc_chrom and regions_overlap(start, end, loc_start, loc_end):
					locus_names.append(locus["Locus"])
		
		locus_info = ', '.join(locus_names) if locus_names else ""
		full_rep_location = chrom + ':' + str(start) + '-' + str(end)
		fully_rep_read_sources.append({"Coordinates": full_rep_location, "Count": count, "Locus": locus_info})
	

	path_repeats_combined = path_repeats_in_spanning_reads + path_repeats_in_flanking_reads # Combine spanning and flanking reads
	path_repeats_in_mate_reads = sorted(set(path_repeats_in_mate_reads)) # Take unique values (i.e. filter duplicate read names out)

	# Set 0-s if no elements
	path_repeats_combined = path_repeats_combined if len(path_repeats_combined) > 0 else 0
	return_repeats_mates = len(path_repeats_in_mate_reads) if path_repeats_in_mate_reads else 0

	# Determine the number of pathogenic repeats in flanking reads
	if path_repeats_combined:
		path_repeats_combined_highest = {}

		for repeats in path_repeats_combined:
			number_of_reads = path_repeats_combined.count(repeats)
			path_repeats_combined_highest[repeats] = number_of_reads
	else:
		path_repeats_combined_highest = {0: 0} # Set zeros if no results

	return_repeats_combined = dict(sorted(path_repeats_combined_highest.items(), key=lambda x: x[1], reverse = True)[:7]) # Get the 7 maximum values to display on the app

	# Close files
	input_file.close()
	output_file.close()

	return tmpbam, return_repeats_combined, return_repeats_mates, offtargets_list, fully_rep_read_sources, genome, genome_has_chr

def findAlignedIndexFile(req_file):
	req_file_index = req_file+'.crai' if req_file.endswith('cram') else req_file+'.bai' # e.g. file.bam.bai

	if not os.path.isfile(req_file_index): # If file.bam.bai does not exist, try another naming for index (e.g. file.bai)
		req_file_index = os.path.splitext(req_file)[0]+'.crai' if req_file.endswith('cram') else os.path.splitext(req_file)[0]+'.bai' # e.g. file.bai

	if os.path.isfile(req_file_index):
		return req_file_index
	else:
		return None

def analyseOneLocus(config, req_locus, req_sex, req_genome, req_reference, req_file, req_output_folder, req_analysis, genome_has_chr, is_custom):
	"""Main function which will iniate the analysis

	Args:
		req_disease (str): User specified name of the disease, or empty when a Locus was selected
		req_locus (str): User specified name of the locus/gene, or empty when a Disease was selected
		req_file (file): Original user's sample BAM or CRAM file
		req_analysis (str): User's requested type of the analysis - either 'standard' or 'extended

	Returns:
		dict: Returned response
	"""

	if is_custom:
		catalog_entry = {
			"Locus": req_locus["LocusID"],
			"LocationCoordinates": {
				req_genome: req_locus["Coordinates"]
			},
			"Motif": req_locus["Motif"],
			"MotifPlusStrand": req_locus["Motif"],
			"RepeatType": "Standard",
			"Diseases": {
				"NA": {
					"DiseaseName": req_locus["Disease"],
					"DiseaseSymbol": "NA",
					"DiseaseOMIM": "",
					"Inheritance": req_locus["Inheritance"],
					"NormalRange": {
						"Min": int(req_locus["NormalRangeMin"]),
						"Max": int(req_locus["NormalRangeMax"])
					},
					"IntermediateRange": "NA",
					"PathogenicCutoff": int(req_locus["PathogenicCutoff"])
				}
			},
			"OfftargetRegions": {},
			"FlankingSequences": {
				"Before": "",
				"After": ""
			},
			"GenomeExceptions": "",
			"PopulationData": {}
		}
	else:
		catalog_entry = readCatalogue(req_locus)

		if not catalog_entry:
			response = {'response': 'error',
						'data': 'Unrecognised locus.'}
			return response

	req_locus_name = req_locus["LocusID"] if is_custom else req_locus

	req_file_index = findAlignedIndexFile(req_file)

	files = [('file', req_file),
			('file', req_file_index)]

	# XYLT1 needs realignment so do extended analysis
	xylt1_req_analysis = req_analysis
	if req_locus == "XYLT1":
		req_analysis = "extended"

	if req_analysis == "standard":
		if config["verbose"]:
			print("-- 4 -- Using the original sequencing file: " + req_file + " for " + req_locus_name + " -- 4 --")
			print("-- 5 -- Running the 'Standard' analysis -- 5 --")
	
		results = analyseSampleFile(config, req_file, files, req_output_folder, req_locus, is_custom, req_sex, req_genome, req_reference, req_analysis, genome_has_chr, repeats_mates='', repeats_flanking='', offtargets='', fully_rep_read_sources=[], catalog_entry=catalog_entry)

		if results:
			response = {'response': 'ok',
						'results': results}
		else:
			response = {'response': 'error',
						'data': 'Unknown error'}
			
	elif req_analysis == "extended":
		if config["extended_analysis"] == "stripy":
			bamfile = tempfile.NamedTemporaryFile(suffix = '.bam').name

			if config["verbose"]:
				print("-- 4 -- Creating a temporary BAM file: " + bamfile + " for " + req_locus_name + " -- 4 --")

			with open(bamfile, 'w') as tmp:
				bam_for_analysis, repeats_in_flanking, repeats_in_mates, offtarget_list, fully_rep_read_sources, genome, genome_has_chr = makeBAM(config, req_file, req_analysis, req_genome, genome_has_chr, tmp, catalog_entry, req_reference)
				bam_file_size = os.stat(bam_for_analysis.name).st_size / 1024 # Generated BAM file size in kilobytes

				# Sort and index the generated BAM file
				pysam.sort('-o', bamfile, bamfile)
				pysam.index(bamfile)

				files = [('file', bamfile), 
						('file', bamfile + '.bai')]

				offtargets_final_list = json.dumps(offtarget_list) if (len(offtarget_list) > 0) else ''

				if req_locus == "XYLT1" and xylt1_req_analysis == "standard":
					offtargets_final_list = ''

				if bam_file_size < 10: # If the generated BAM file is less than 10 kb then it means it does not contain any reads
					if config["verbose"]:
						print("-- 5 -- BAM file size: " + str(round(bam_file_size, 1)) + " kb <-- NO READS near the " + req_locus_name + " locus within " + str(config['fregion_length_bp']) + " bp -- 5 --")

					response = {'response': 'error',
								'data': 'NoReadsExtractedOut'}
				else:
					if config["verbose"]:
						print("-- 5 -- Running the 'Extended' analysis on BAM file with a size of " + str(round(bam_file_size, 1)) + " kb for " + req_locus_name + " -- 5 --")
					
					results = analyseSampleFile(config, req_file, files, req_output_folder, req_locus, is_custom, req_sex, req_genome, req_reference, req_analysis, genome_has_chr, repeats_in_mates, repeats_in_flanking, offtargets_final_list, fully_rep_read_sources, catalog_entry)

					if results:
						response = {'response': 'ok',
									'results': results}
					else:
						response = {'response': 'error',
									'data': 'Unknown error'}

		elif config['extended_analysis'] == "eh_only":
			if config["verbose"]:
				print("-- 4 -- Using the original sequencing file: " + req_file + " for " + req_locus_name + " -- 4 --")
				print("-- 5 -- Running the 'Extended' analysis (direct-ExpansionHunter) -- 5 --")
	
			# Find offtargets
			offtarget_list = findOfftargets(req_file, req_reference, genome_has_chr, req_genome, config, catalog_entry)
			offtargets_final_list = json.dumps(offtarget_list) if (len(offtarget_list) > 0) else ''

			if req_locus != "XYLT1":
				# XYLT1 cannot be directly analysed with EH
				results = analyseSampleFile(config, req_file, files, req_output_folder, req_locus, is_custom, req_sex, req_genome, req_reference, req_analysis, genome_has_chr, repeats_mates='', repeats_flanking='', offtargets=offtargets_final_list, fully_rep_read_sources=[], catalog_entry=catalog_entry)
			else:
				results = None

			if results:
				response = {'response': 'ok',
							'results': results}
			else:
				response = {'response': 'error',
							'data': 'Unknown error'}

	
	return response


def executeSingleAnalysis(locus, args, config, genome_has_chr, is_custom):
	""" Analyses one locus

	Args:
		locus (str): Name of locus
		args (str): Other metadata of analysis

	Returns:
		dict: Results for the analysed locus
	"""
	locusID = locus["LocusID"] if is_custom else locus

	if config["verbose"]:
		print("-- 3 -- Taking the " + locusID + " locus -- 3 --")

	analysis_response = analyseOneLocus(config, locus, args.sex, args.genome, args.reference, args.input, args.output, args.analysis, genome_has_chr, is_custom)

	if analysis_response["response"] == "error":
		error_message = "(" + locusID + ") Error: Could not analyse the locus"
		if analysis_response["data"]:
			error_message += " (" + analysis_response["data"] + ")"

		# In case there were no reads near the STR locus then show this in the final results too
		if analysis_response["data"] == "NoReadsExtractedOut":
			catalog_entry = getDiseaseInfo(locusID)
			new_analysis_response = { 
						locusID: {
							"TargetedLocus": {
								"LocusID": locusID,
								"Coordinates": catalog_entry['LocationCoordinates'][args.genome],
								"Motif": catalog_entry["MotifPlusStrand"],
								"CorrespondingDisease": catalog_entry['Diseases'],
							},
							"Flag": -1,
							"Filter": "NoCoverageAtOrNearSTRLocus"
						}
					}

			analysis_response["results"] = new_analysis_response
		else:
			# Log an error as 'NA' flag (append into a file)
			if args.logflags and config["log_flag_threshold"] == -1:
				with open(args.logflags, 'a') as outputLogFile:
					outputLogFile.write(args.input + "\t" + locusID + "\tNA\n")
			return None

	# Log flags (append into a file)
	logflag_flag = analysis_response["results"][locusID]["Flag"]
	if args.logflags:
		if logflag_flag >= config["log_flag_threshold"] or (logflag_flag == -1 and config["log_flag_threshold"] == -1):
			with open(args.logflags, 'a') as outputLogFile:
				logflag_flag = logflag_flag if logflag_flag > -1 else "NA"
				outputLogFile.write(args.input + "\t" + locusID + "\t" + str(logflag_flag) + "\n")

	return analysis_response["results"]
	


def jsonToTsv(json_content):
	json_obj = json.loads(json_content)

	tsv_lines = []
	header = [
		"Locus", "Coordinates", "Motif", "Coverage", 
		"Allele1_Repeats", "Allele2_Repeats", 
		"Allele1_CI", "Allele2_CI", 
		"Allele1_IsOutlier", "Allele2_IsOutlier", 
		"Allele1_DiseaseRange", "Allele2_DiseaseRange", 
		"CorrespondingDiseases", "Filter"
	]

	tsv_lines.append("\t".join(header))
	for genotyping_result in json_obj["GenotypingResults"]:
		for key, value in genotyping_result.items():
			targeted_locus = value["TargetedLocus"]
			locus_id = targeted_locus["LocusID"]
			coordinates = targeted_locus["Coordinates"]
			motif = targeted_locus["Motif"]
			filter_message = value["Filter"]

			if filter_message == "PASS":
				num_alleles = len(value["Alleles"])
				coverage = str(value["Metadata"]["Coverage"])
				allele_0_repeats = str(value["Alleles"][0]["Repeats"])
				allele_0_is_outlier = str(value["Alleles"][0]["IsPopulationOutlier"])
				allele_0_range = value["Alleles"][0]["Range"]
				allele_0_ci_combined = str(value["Alleles"][0]["CI"]["Min"]) + "-" + str(value["Alleles"][0]["CI"]["Max"])

				if num_alleles == 2:
					allele_1_repeats = str(value["Alleles"][1]["Repeats"])
					allele_1_is_outlier = str(value["Alleles"][1]["IsPopulationOutlier"])
					allele_1_range = value["Alleles"][1]["Range"]
					allele_1_ci_combined = str(value["Alleles"][1]["CI"]["Min"]) + "-" + str(value["Alleles"][1]["CI"]["Max"])
				else:
					allele_1_repeats = "NA"
					allele_1_is_outlier = "NA"
					allele_1_range = "NA"
					allele_1_ci_combined = "NA"

				disease_symbols = []
				dis_in = targeted_locus.get("CorrespondingDisease", {})
				if isinstance(dis_in, dict):
					for disease_key, disease_info in dis_in.items():
						if isinstance(disease_info, dict) and "DiseaseSymbol" in disease_info:
							disease_symbols.append(disease_info["DiseaseSymbol"])

				corresponding_diseases = ", ".join(disease_symbols)

				tsv_line = (
					f"{locus_id}\t{coordinates}\t{motif}\t{coverage}\t{allele_0_repeats}\t{allele_1_repeats}\t"
					f"{allele_0_ci_combined}\t{allele_1_ci_combined}\t"
					f"{allele_0_is_outlier}\t{allele_1_is_outlier}\t{allele_0_range}\t{allele_1_range}\t"
					f"{corresponding_diseases}\t{filter_message}"
				)
				tsv_lines.append(tsv_line)

			else:
				tsv_line = (
					f"{locus_id}\t{coordinates}\t{motif}\t\t\t\t\t\t\t\t\t\t\t{filter_message}"
				)
				tsv_lines.append(tsv_line)

		
	return "\n".join(tsv_lines)+"\n"

def runWholeAnalysis(args):
	"""Run the whole analysis for the inputted sample

	Args:
		args (args): User specified arguments
	"""

	if config["verbose"]:
		print("-- 1 -- Preparing the analysis -- 1 --")
	try:
		# Determine whether file is BAM or CRAM before starting to analyse, if not then throw an error
		input_file = args.input
		if not input_file.lower().endswith(('.bam', '.cram')):
			print("Error: Specified file (" + args.input + ") is not BAM or CRAM format.")
			return None

		# Get known loci
		if len(args.locus) > 0:
			if "," in args.locus:
				loci = args.locus.split(",")
			else:
				loci = [args.locus]

			# Since RFC1 has two pathogenic motif, we shall add them both
			if "RFC1" in loci:
				loci.remove("RFC1")
				loci.append("RFC1-AAGGG")
				loci.append("RFC1-ACAGG")
		else:
			loci = []

		# Get custom loci
		custom_loci = []
		if args.custom:
			with open(args.custom, newline = '') as bedfile:
				custom_bed = csv.reader(bedfile, delimiter = '\t')
				for cl in custom_bed:
					if int(cl[2]) > int(cl[1]) and int(cl[2])-int(cl[1]) < 500: # If end is more than start and the region is less than 500 bp
						coorinates = cl[0]+':'+cl[1]+'-'+cl[2]
						motif = cl[3]
						locusID = cl[4] if len(cl) >= 5 else "Custom"

						if len(cl) >= 9:
							disease = cl[5]
							inheritance = cl[6]
							normal_range = cl[7].split("-") if "-" in cl[7] else [cl[7], cl[7]]
							pathogenic_cutoff = cl[8]
						else:
							disease = "NA"
							inheritance = "NA"
							normal_range = [-1, -1]
							pathogenic_cutoff = 9999

						custom_loci.append({
							"LocusID": locusID, 
							"Coordinates": coorinates,
							"Motif": motif,
							"Disease": disease,
							"Inheritance": inheritance,
							"NormalRangeMin": normal_range[0],
							"NormalRangeMax": normal_range[1],
							"PathogenicCutoff": pathogenic_cutoff
							})

		is_custom = True if len(custom_loci) > 0 else False
		custom_loci_list = list(set(([x["LocusID"] for x in custom_loci]))) if is_custom else []

		time_now = datetime.now().strftime("%d.%m.%Y %H:%M")
		input_sample_name = args.input.split("/")[-1]

		results = {
			"JobDetails": {
				"SoftwareVersion": 'STRipy v' + __version__,
				"AnalysisType": args.analysis.title(),
				"TimeOfAnalysis": time_now,
				"InputFile": os.path.abspath(args.input),
				"OutputFiles": os.path.abspath(args.output),
				"TargetedLoci": loci+custom_loci_list,
				"SampleSex": args.sex.title(),
				"Reference": {
					"Genome": args.genome,
					"File": args.reference
				},
			},
			"GenotypingResults": []
		}

		# If number of threads is specified in config file, use it, otherwise take the maximum number of threads
		num_threads = config['num_threads'] if isinstance(config['num_threads'], int) else multiprocessing.cpu_count()
		pool = multiprocessing.Pool(processes = num_threads) # Run on all available cores

		if config["verbose"]:
			print("-- 2 -- Executing analysis on " + str(num_threads) + " threads -- 2 --")

		genome_has_chr = checkChromNames(args.input, args.reference)

		genotyping_results_known = pool.map(partial(executeSingleAnalysis, args = args, config = config, genome_has_chr = genome_has_chr, is_custom = False), loci) if loci else []
		genotyping_results_custom = pool.map(partial(executeSingleAnalysis, args = args, config = config, genome_has_chr = genome_has_chr, is_custom = True), custom_loci) if is_custom else []

		genotyping_results_pooled = genotyping_results_known + genotyping_results_custom
		for r in genotyping_results_pooled:
			if r:
				results["GenotypingResults"].append(r)

		if config["verbose"]:
			print("-- 14 -- Generating the final report -- 14 --")

		# Generate results in JSON
		results_json = json.dumps(results, sort_keys = True, indent = 4)
		results_json_hash = hashlib.md5(results_json.encode("utf-8")).hexdigest()
		results["Checksum"] = results_json_hash
		output_json = json.dumps(results, sort_keys = True, indent = 4)
		output_tsv = jsonToTsv(output_json)

		# Save results to a JSON file
		if config["output_json"]:
			with open(os.path.join(args.output, input_sample_name + ".json"), 'wt') as outputJSONfile:
				outputJSONfile.write(output_json)

		# Save results to a TSV file
		if config["output_tsv"]:
			with open(os.path.join(args.output, input_sample_name + ".tsv"), 'wt') as outputTSVfile:
				outputTSVfile.write(output_tsv)

		# Save results to a HTML file
		if config["output_html"]:
			with open(os.path.join(scriptpath, 'results_template.html'), 'rt') as resultsHTMLtemplate:
				with open(os.path.join(args.output, input_sample_name + ".html"), 'wt') as outputHTMLfile:
					for line in resultsHTMLtemplate:
						outputHTMLfile.write(line.replace('/*SampleResultsJSON*/', output_json))
		return True
	except:
		if config["verbose"]:
			raise
		else:
			pass
		return False

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--input",		required = True, 																	help = "Input file (indexed BAM or CRAM)")
	parser.add_argument("--locus",		required = False, 											default = "",			help = "Loci to genotype")
	parser.add_argument("--custom",		required = False, 											default = "",			help = "Custom loci file path (BED file)")
	parser.add_argument("--sex",		required = False,	choices = ["male", "female"],			default = "undefined",	help = "Sex of the sample")
	parser.add_argument("--genome",		required = False,	choices = ["hs1", "hg38", "hg19"],		default = "hg38",		help = "Name of the reference genome")
	parser.add_argument("--reference",	required = True,											default = "",			help = "Reference genome (FASTA file)")
	parser.add_argument("--analysis",	required = False,	choices = ["standard", "extended"],		default = "standard",	help = "Analysis type")
	parser.add_argument("--output",		required = False,											default = "results/",	help = "Output folder")
	parser.add_argument("--logflags",	required = False,											default = "",			help = "File path of the output log file")
	parser.add_argument("--config",		required = False,											default = "",			help = "File path of the config file")
	args = parser.parse_args()

	config_file = args.config if args.config else os.path.join(scriptpath, 'config.json')

	# Check that all tools, files and folders exist
	missing_components = []

	if not os.access(config_file, os.R_OK):
		missing_components.append("Config file: config.json")
	else:
		config = loadConfiguration(config_file)
		if not executableFileExists(config["filepath_expansionhunter"]): missing_components.append("ExpansionHunter: " + config["filepath_expansionhunter"])
		if not executableFileExists(config["filepath_reviewer"]): missing_components.append("REViewer: " + config["filepath_reviewer"])
		if not executableFileExists(config["filepath_samtools"]): missing_components.append("Samtools: " + config["filepath_samtools"])
		if not os.access(os.path.join(scriptpath, 'catalog.json'), os.R_OK): missing_components.append("Catalog file: catalog.json")
		if not os.access(os.path.join(scriptpath, 'results_template.html'), os.R_OK): missing_components.append("Results template file: results_template.html")
		if not os.access(args.input, os.R_OK): missing_components.append("Input file: " + args.input)
		if not findAlignedIndexFile(args.input): missing_components.append("Index for the input file (.bai or .crai)")
		if not os.access(args.reference, os.R_OK): missing_components.append("Reference file: " + args.reference)
		if not os.access(args.output, os.W_OK): missing_components.append("Output folder: " + args.output)

	if missing_components:
		error = True
		print("Pre-analysis check failed! The following files could not be found or has no read/write access: \n--> " + "\n--> ".join(missing_components), file=sys.stderr)
		sys.exit(1)
	else:
		if args.locus or args.custom:
			print("Starting analysis (" + args.input + ')')
			analysis_done = runWholeAnalysis(args)
			if analysis_done:
				print("Analysis finished (" + args.input + ')')
				sys.exit(0)
			else:
				print("Error in analysis (" + args.input + ')', file=sys.stderr)
				sys.exit(1)
		else:
			print("Loci not defined", file=sys.stderr)
			sys.exit(1)
